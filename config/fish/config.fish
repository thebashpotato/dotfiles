# kill fish greeting
set fish_greeting

# Custom key bindings
# launch sefr browser search when Control + s is pressed
bind \cs sefr

# launch the 'lf' terminal file manager when Control + l is pressed
bind \cl ranger

# launch ipython interactive
bind \cp ipython

# launch vsm open Alt + o
bind \eo 'vsm open'

# launch vsm remove Alt + r
bind \er 'vsm remove'

# launch vsm list Alt + l
bind \el 'vsm list'

############# Linux environment variables ###################

# Set globals
set -x EDITOR nvim
set -x VISUAL nvim
set -x BROWSER firefox
set -x READER zathura

# XDG stuff
set XDG_CONFIG_HOME "$HOME/.config"
set XDG_DATA_HOME "$HOME/.local/share"
set XDG_CACHE_HOME "$HOME/.cache"

# Fix Gtk Apps
set XDG_DATA_DIRS /usr/share $XDG_DATA_DIRS

# Fix flatpak
set XDG_DATA_DIRS /var/lib/flatpak/exports/share $XDG_DATA_DIRS
set XDG_DATA_DIRS "$XDG_DATA_HOME/flatpak/exports/share" $XDG_DATA_DIRS

# add .local/bin to $PATH
set PATH "$HOME/.local/bin" $PATH

############# Custom Environment Variables ###################

# Neovide
set -Ux WINIT_UNIX_BACKEND x11

# Vim sessions directory
set -Ux VIM_SESSIONS "$XDG_CONFIG_HOME/vim_sessions"

# Set virtual fish home
set -Ux VIRTUALFISH_HOME "$XDG_CONFIG_HOME/virtualenvs"

# Set CPM cache directory
set -Ux CPM_SOURCE_CACHE "$HOME/.cache/CPM"

############# Custom Tooling ###################

# GNU Linker
set LD_LIBRARY_PATH /usr/local/lib $LD_LIBRARY_PATH

# Rust Tool chain
set PATH "$HOME/.cargo/bin" $PATH
set PATH "$XDG_DATA_HOME/cargo/bin" $PATH
set CARGO_HOME "$HOME/.cargo"

# Go tool chain
set PATH /usr/local/go/bin $PATH
set GOPATH "$HOME/Development/go"

# Flutter tool chain
set PATH "$HOME/Tools/flutter/bin" $PATH

# Android studio
set PATH "$HOME/Tools/android-studio/bin" $PATH

# Nvim nightly dev build
set PATH /opt/nvim-linux64/bin $PATH

############# Misc ###################

if status is-interactive
    # workaround for https://github.com/fish-shell/fish-shell/issues/3481
    function fish_vi_cursor
    end
    fish_vi_key_bindings
    bind -M insert jk "if commandline -P; commandline -f cancel; else; set fish_bind_mode default; commandline -f backward-char force-repaint; end"

    starship init fish | source

    # custom function from ~/.config/fish/functions/fix_ssh.fish
    fix_ssh
end
