# Nasty work around function to ssh-agent issue
function fix_ssh
    if pgrep --full ssh-agent &>/dev/null
        # it is running, which means we need to kill it and restart it
        kill (pgrep ssh-agent)
    end
    eval (ssh-agent -c) &>/dev/null
    ssh-add ~/.ssh/github &>/dev/null
end
