# dotfiles

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

My configuration files

## Table of Contents

- [Install](#install)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

### Fonts

```bash
mkdir -p ~/.local/share/fonts
cp -rv fonts/Nerdfonts/* ~/.local/share/fonts
fc-cache -vf
```

## Maintainers

[@thebashpotato](https://gitlab.com/thebashpotato)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Matt Williams
